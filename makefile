
# Directories.
BINARIES = .
SOURCES  = .
OBJECTS  = obj

# Files 
SRCFILES = ./atlas.cpp ./main.cpp ./external/lodepng.cpp
HDRFILES = external/lodepng.h
OBJFILES = $(patsubst $(SOURCES)/%.cpp, $(OBJECTS)/%.o, $(SRCFILES))

#$(patsubst pattern,replacement,text)

# PCH.
PCHSRCFILE = precompiled.h
PCHHEADERS = external/lodepng.h

#
CXX = g++

# Compilation flags
CXXFLAGS     = -Wall -std=c++11 -O3
CXXFLAGS_PCH = -include $(PCHSRCFILE) -Winvalid-pch $(CXXFLAGS)

# Libraries
LDFLAGS = 

# exexutable file
BIN = dir2atlas

#http://www.gnu.org/software/make/manual/make.html#Automatic-Variables

$(BINARIES)/$(BIN): $(OBJFILES)
	@echo "$(CXX) $(OBJFILES) -o $(BINARIES)/$(BIN) $(LDFLAGS)"
	@$(CXX) $(OBJFILES) -o $(BINARIES)/$(BIN) $(LDFLAGS)

$(OBJECTS)/%.o: $(SOURCES)/%.cpp $(PCHSRCFILE).gch directories
	@echo "$(CXX) -c $(CXXFLAGS) -o $@ $<"
	@$(CXX) -c $(CXXFLAGS) -o $@ $<

$(PCHSRCFILE).gch: $(PCHSRCFILE)
	@echo "$(CXX) -c $(CXXFLAGS) $(PCHSRCFILE)"
	@$(CXX) -c $(CXXFLAGS) $(PCHSRCFILE)

test: loader_test

loader_test: $(OBJECTS)/loader/main.o $(OBJECTS)/loader/loader.o
	@echo "$(CXX) $^ -o $(BINARIES)/loader_test $(LDFLAGS)"
	@$(CXX) $^ -o $(BINARIES)/loader_test $(LDFLAGS)

$(OBJECTS)/loader/%.o: loader/%.cpp directories
	@echo "$(CXX) -c $(CXXFLAGS) -o $@ $<"
	@$(CXX) -c $(CXXFLAGS) -o $@ $<

directories:
	mkdir -p $(OBJECTS)
	mkdir -p $(OBJECTS)/external
	mkdir -p $(OBJECTS)/loader
	
clean:
	rm -rf $(OBJECTS)
	rm -f $(BINARIES)/$(BIN)
	rm -f $(PCHSRCFILE).gch
	rm -f $(BINARIES)/loader_test

