/*
Copyright (c) 2013 Paweł Turkowski

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
*/

#pragma once
#include "../precompiled.h"

#include <vector>

#include "../atlas.h"

namespace atlas{

  struct Loader
  {
    struct Line
    {
      int32_t width, height;
      std::string name;
      float txX, txY;
      float txW, txH;
    };
	
  protected:
    Loader() :
      buffer2(),
      readIdx(-1)
    {}
    
  public:
    void open(const char* name);
    
    void rewind();
    
    bool isOk() const;
    
    std::vector<char> buffer2;
    uint32_t readIdx;
    Header header;
  };

  struct TextLoader : Loader
  {
    TextLoader() = default;
    
    TextLoader(const char* name);
    
    ~TextLoader() =default;

    Header readHeader();

    bool readLine(Line* line);
  };

  struct BinaryLoader : Loader
  {
    BinaryLoader() = default;
    
    BinaryLoader(const char* name) :
      Loader()
    {
      open(name);
    };

    ~BinaryLoader() = default;

    Header readHeader();
    
    bool readLine(Line* line);
  };

}//namespace
