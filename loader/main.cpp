/*
Copyright (c) 2013 Paweł Turkowski

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
*/

#include "../precompiled.h"

#include <string>

#include "loader.h"

template<class Loader>
void testLoader1(const char* name)
{
  Loader loader(name);
  atlas::Header header = loader.readHeader();
  printf("Header: %u %u %u %f %f\n", header.tilesCount, header.imgWidth, header.imgHeight,
         header.txOffsetX, header.txOffsetY);
  atlas::Loader::Line line;
  
  while(loader.readLine(&line))
  {
    printf("%s %f %f %f %f %u %u\n", line.name.c_str(), line.txX, line.txY, line.txW, line.txH, line.width, line. height);
    
    int width = header.imgWidth;
    int height = header.imgHeight;
    int level = 1;
    while(width > 1 || height > 1)
    {
      printf("  %i: %i %i %i %i\n", level, (int)(line.txX * width), (int)(line.txY * height),
              (int)(line.txW * width), (int)(line.txH * height));
      width /= 2;
      height /= 2;
      ++level;
    }
  }
}

int main(int argc, char* argv[])
{
  if(argc < 2)
  {
    return 1;
  }
  
  std::string baseName = argv[1];
  
  printf("base name: %s\n", argv[1]);
  
  puts("Testing text loader:");
  testLoader1<atlas::TextLoader>((baseName + ".txc.txt").c_str());
  
  puts("\nTesting binary loader:");
  testLoader1<atlas::BinaryLoader>((baseName + ".txc.bin").c_str());
  
  return 0;
}//main
