/*
Copyright (c) 2013 Paweł Turkowski

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
*/

#include "../precompiled.h"

#include <array>

#include "loader.h"
#include "../common.h"

using namespace atlas;

void Loader::open(const char* name)
{
  loadFile(name, buffer2);
  if(buffer2.size() != 0)
  {
    buffer2.push_back(0);
    rewind();
  }
  
}

void Loader::rewind()
{
  readIdx = 0;
}

bool Loader::isOk() const
{
  return readIdx < buffer2.size();
}
    
//const char TextLoader::taiFormat[] = "%"QUOTE(MAX_CSTRING_LEN)"s %*s %u, %"QUOTE(ATLAS_TYPE_LEN)"s %f, %f, %f, %f, %f";

template<class T, int N>
int skipTo(std::array<T,N>&& c, const std::vector<T>& buffer, uint32_t curPos)
{
  while(curPos < buffer.size())
  {
    for(auto ic : c)
    {
      if(ic == buffer[curPos])
        return curPos;
    }
    ++curPos;
  }
  return curPos;
}

template<class T>
int skipTo(std::initializer_list<T> list, const std::vector<T>& buffer, uint32_t curPos)
{
  while(curPos < buffer.size())
  {
    for(auto ic : list)
    {
      if(ic == buffer[curPos])
        return curPos;
    }
    ++curPos;
  }
  return curPos;
}

template<class T, size_t N>
int skipToFirstNon(std::array<T,N>&& c, const std::vector<T>& buffer, int curPos)
{
  while(curPos < buffer.size())
  {
    bool found = false;
    for(auto ic : c)
    {
      if(ic == buffer[curPos])
      {
        found = true;
        break;
      }
    }
    ++curPos;
    if(!found)
      return curPos;
  }
  return curPos;
}

template<class T>
int skipUnwanted(const std::vector<T>& buffer, int curPos)
{
  while(curPos < buffer.size())
  {
    char c = buffer[curPos];
    switch(c)
    {
    case '#':
      curPos = skipTo({'\n'}, buffer, curPos) + 1;
      break;
    case '\n':
    case '\t':
    case ' ':
      curPos += 1;
      break;
    case EOF:
      return false;
    default:
      return curPos;
    }
  }
  return curPos;
}


std::string removeQuotes(const char* str)
{
  //TODO: detect white space, and skip it
  std::string out;
  if(str[0] == '"')
    out = &str[1];
  
  if(out.size() > 0 && out.back() == '"')
    out.pop_back();
  
  return out;
}

TextLoader::TextLoader(const char* name) : 
  Loader()
{
  open(name);
}

Header TextLoader::readHeader()
{
  int curPos = skipUnwanted(buffer2, 0);
  
  int result = sscanf(&buffer2[curPos], "%u %u %u %f %f %u", &header.tilesCount, &header.imgWidth, 
                      &header.imgHeight, &header.txOffsetX, &header.txOffsetY,
                      &header.flags);
  if(result != 6)
  {
    header.tilesCount = 0;
    header.imgWidth = header.imgHeight = 0;
    return header;
  }
  
  if(readIdx == 0)
  {
    readIdx = skipTo({'\n'}, buffer2, curPos) + 1;
  }
  return header;
}

bool TextLoader::readLine(Line* line)
{
  if(!isOk())
  {
    return false;
  }
  
  if(readIdx == 0)
  {
    readHeader();
  }
  
  readIdx = skipUnwanted(buffer2, readIdx);
  
  char filename[256];
  
  int scannedCount = sscanf(&buffer2[readIdx], "%255s %f %f %f %f", filename,
                            &line->txX, &line->txY, &line->txW, &line->txH); 
  if(scannedCount != 5)
  {
    return false;
  }
  
  line->name = removeQuotes(filename);
  line->width = (line->txW + header.txOffsetX) * header.imgWidth;
  line->height = (line->txH + header.txOffsetY) * header.imgHeight;
  
  readIdx = skipTo({'\n'}, buffer2, readIdx) + 1;
  
  return true;
}

Header BinaryLoader::readHeader()
{
  assert(buffer2.size() > sizeof(Header) );

  int last = 0;
  last += readValue(&header.tilesCount, &buffer2[last]);
  
  last += readValue(&header.imgWidth,  &buffer2[last]);
  last += readValue(&header.imgHeight, &buffer2[last]);
  
  last += readValue(&header.txOffsetX, &buffer2[last]);
  last += readValue(&header.txOffsetY, &buffer2[last]);
  
  last += readValue(&header.flags,&buffer2[last]);
  
  last += 1;	// skip '\n'
  
  if(readIdx == 0)
  {
    readIdx = last;
  }
  
  return header;
}

bool BinaryLoader::readLine(Line* line)
{
  if(!isOk())
  {
    return false;
  }
  
  if(readIdx == 0)
  {
    readHeader();
  }
  
	if (readIdx >= buffer2.size()-1 )
		return false;
	
  line->name = removeQuotes(&buffer2[readIdx]);
  readIdx += line->name.size();
  readIdx = skipTo({'\0'}, buffer2, readIdx) + 1;
	
	//TODO: make it endian - independent:  
  readIdx += readValue(&line->txX, &buffer2[readIdx] );
  readIdx += readValue(&line->txY, &buffer2[readIdx] );
  readIdx += readValue(&line->txW, &buffer2[readIdx] );
  readIdx += readValue(&line->txH, &buffer2[readIdx] );
  
  line->width = (line->txW + header.txOffsetX) * header.imgWidth;
  line->height = (line->txH + header.txOffsetY) * header.imgHeight;
  
  readIdx += 1; // skip new line character '\n';
  
  return true;
}
