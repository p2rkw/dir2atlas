/*
Copyright (c) 2013 Paweł Turkowski

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
*/

#include "precompiled.h"

#include <cassert>
#include <cstdint>
#include <cctype>

#include <dirent.h>

#include "external/lodepng.h"
#include "atlas.h"

#define PRINT( what, format) printf( QUOTE(what) " = " format, what )

struct Directory
{
  struct Iterator
  {
    dirent* operator*()
    {
      return entry;
    }

    Iterator& operator++()
    {
      entry = readdir(dp);
      return *this;
    }

    bool operator ==(const Iterator& it)
    {
      return this->dp == it.dp && this->entry == it.entry;
    }

    bool operator !=(const Iterator& it)
    {
      return !operator==(it);
    }

    DIR* dp;
    dirent* entry;
  };

  Directory(const char* path)
  {
    dp = opendir(path);
    if(dp == NULL) {
      perror("opendir");
      isOk = false;
    }
    isOk = true;
  }
  ~Directory()
  {
    if(dp)
      closedir(dp);
  }

  Iterator begin()
  {
    return {dp, readdir(dp)};
  }

  Iterator end()
  {
    return {dp, 0};
  }


  DIR* dp;
  bool isOk;
};

int myStrCaseCmp(const char* left, const char* right)
{
  while(*left != 0 && *right != 0)
  {
    char l = tolower(*left);
    char r = tolower(*right);
    
    if(l != r)
    {
      return l - r;
    }
    
    ++left;
    ++right;
  }
  
  return tolower(*left) - tolower(*right);
}


#define SEPARATOR '/'

struct CommandLineSettings
{
  uint32_t id;
  const char* longName;
  const char* shortName;
  const char* argsFormat;
  const char* description;
};

enum CommandLineSettingsNames
{
  INVALID_OPTION,

  HALF_TEXEL,
  NO_IMAGE,
  ADD_BORDER,
  OUTPUT,
};


CommandLineSettings clSettings[] = {
  { HALF_TEXEL, "--half-texel", "-ht", "",
    "adds half texel to x offset and y offset and substract one texel from width and height"
  },
  { NO_IMAGE, "--no-image", "-ni", "", "generate only texcoords info" },
  { ADD_BORDER, "--add_border", "-ab", "", "add 1px border to texture" },
  { OUTPUT, "--output", "-o", "%s", "specify output file base name" },
  { OUTPUT, "--directory", "-d", "%s", "specify input directory" },
  { 0, "", "", "", "" },
};

bool addHalfTexel = false;
bool dontGenerateImage = false;
bool addBorder = false;


bool isOption(const char* str, int32_t optIndex)
{
  if((myStrCaseCmp(str, clSettings[optIndex].longName) == 0) ||
      (myStrCaseCmp(str, clSettings[optIndex].shortName) == 0))
  {
    printf("enabled option: %s\n", clSettings[optIndex].longName);
    return true;
  }
  return false;
}

void missingArgumentInfo(int32_t i)
{
  critError(format("option '%s' requires an argument of type %s\n",
                   clSettings[i].longName, clSettings[i].argsFormat));
}

/*
false - wrong name
true - good name
*/
bool checkPngFilename(const char* filename)
{
  int len = strlen(filename);
  return !(len <= 4 || (myStrCaseCmp(filename + len - 4, ".png") != 0));
}

int main(int argc, char* argv[])
{
# ifdef _DEBUG
  puts("Dont use debug version of atlas generator!");
  for(int i = 0; i < (int)argc; ++i) {
    printf("argv[%i] = %s\n", i, argv[i]);

  }/*for (i)*/
  PRINT(argc, "%i\n");
# endif

  if(argc < 2) {
    printf("Usage: %s [options]\n", argv[0]);
    puts("\tdirectory is the name of the directory containing the files");
    puts("options:");
    for(int i = 0; clSettings[i].id != 0; ++i)
    {
      printf("\t %s or %s", clSettings[i].longName, clSettings[i].shortName);
      if(clSettings[i].description)
        printf(" - %s\n", clSettings[i].description);
    }/*for (i)*/

    critError("enter to exit...");
    return -1;
  }

  std::string outputName;
  std::string directoryName;
  std::vector<std::string> filesList;

  for(int i = 1; i < argc; ++i)
  {
    int o = 0;
    if(isOption(argv[i], o++))
    {
      addHalfTexel = true;
    }
    else if(isOption(argv[i], o++))
    {
      dontGenerateImage = true;
    }
    else if(isOption(argv[i], o++))
    {
      addBorder = true;
    }
    else if(isOption(argv[i], o++))
    {
      ++i;
      if(i < argc)
        outputName = argv[i];
      else
        missingArgumentInfo(i - 1);

    }
    else if(isOption(argv[i], o++))
    {
      ++i;
      if(i < argc)
        directoryName = argv[i];
      else
        missingArgumentInfo(i - 1);
    }
    else
    { //no more options, start scanning files
      const char* fn = argv[i];
      if(checkPngFilename(fn))
      {
        filesList.push_back(fn);
      }
      else
      {
        printf("only valid png files are accepted, '%s' will not be included\n", fn);
      }
    }

  }/*for (i)*/

  if(filesList.size() == 0)
  { //then load all files in directory
    if(directoryName.size() == 0)
    {
      critError("please specify directory path, and/or files names");
    }

    Directory d1(directoryName.c_str());
    if(!d1.isOk)
    {
      critError(format("directory '%s' not found", argv[1]));
    }
    for(dirent* file : d1)
    {
      const char* fn = file->d_name;
      if(checkPngFilename(fn))
      {
        filesList.push_back(fn);
      }
      else
      {
        printf("only valid png files are accepted, '%s' will not be included\n", fn);
      }
    }
  }

  if(filesList.size() == 0)
  {
    critError("no files to convert");
  }

  if(directoryName.size())
  {
    for(uint32_t i = 0; i < filesList.size(); ++i)
    {
      std::string tmp = filesList[i];
      filesList[i] = directoryName;
      filesList[i] += SEPARATOR;
      filesList[i] += tmp;
    }/*for (i)*/
  }

  if(outputName.size() == 0 && dontGenerateImage == false)
  {
    if(directoryName.size())
      outputName = directoryName;
    else
      critError("please specufy output name (-o)");
  }


  //PRINT( argv[1],"%s\n" );

  using namespace atlas;
  Atlas atlas;

  atlas.setFlags(Atlas::ADD_BORDER, addBorder);
  atlas.setFlags(Atlas::REDUCE_BY_HALF_TEXEL, addHalfTexel);

  for(uint32_t i = 0; i < filesList.size(); ++i)
  {
    atlas.insertTexture(filesList[i]);
  }/*for (i)*/

  atlas.saveTextTexcoords(std::string(outputName + ".txc.txt").c_str());
  atlas.saveBinaryTexcoords(std::string(outputName + ".txc.bin").c_str());

  if(dontGenerateImage == false)
  {
    //atlasBuffer.filename = imageName;
    printf("saving atlas .png file to '%s.png'\n", outputName.c_str());
    atlas.saveImage(std::string(outputName + ".png").c_str());
    puts("save done.\n");
  }

  //mipmap first level
  int mipLevel = 1;
  Texture mipAtlasTex;
  while(atlas.generateMipmap(mipLevel, mipAtlasTex))
  {
    const char* name = format("%s.%i.png", outputName.c_str(), mipLevel);
    mipAtlasTex.savePng(name);
    ++mipLevel;
    printf("mipmap '%s' saved\n", name);
  }

  return 0;
}
