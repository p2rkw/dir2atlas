/*
Copyright (c) 2013 Paweł Turkowski

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
*/

#include "precompiled.h"
#include "atlas.h"

#include <new>
#include <cmath>

#include "external/lodepng.h"

#include "common.h"


using namespace atlas;

Float2::Float2() : i(0.f), f(0.f)
{
}

Float2::Float2(float a)
{
  f = std::modf(a, &i);
}

Float2::Float2(float integral, float factorial) : i(integral), f(factorial)
{
  assert(f < 1.f && f >= 0.f);
}


Texture::Texture() :
  width(0),
  height(0),
  borderWidth(0)
{
}

bool Texture::loadPng(const char* filename)
{
  assert(image.size() == 0);

	uint32_t width, height;
  unsigned error = lodepng::decode(image, width, height, filename);
  this->width = width;
  this->height = height;

  //if there's an error, display it
  if(error != 0) {
    //critError(format("error while loading '%s' file. Is it .png file?", filename));
    printf("error while loading '%s' file. Is it .png file?\n", filename);
    return false;
  }
  else {
    this->filename = filename;
  }
  return true;
}

void Texture::savePng(const char* filename)
{
  //assert(filename.length());
  assert(image.size());
  assert(width);
  assert(height);

  if(filename && filename[0] != 0)
  {
    int len;
    assert((len = strlen(filename), len > 4 &&
            filename[len - 4] == '.' && filename[len - 3] == 'p' &&
            filename[len - 2] == 'n' && filename[len - 1] == 'g'));

    this->filename = filename;
  }

  lodepng::encode(this->filename, image, width, height);
}

void Texture::allocate(int aWidth, int aHeight) 
{
  width = aWidth;
  height = aHeight;
  
  assert(image.size() == 0);
  image.reserve(width * height * 4);
}

void Texture::blitTo(Texture& dest, int32_t destStartX, int32_t destStartY)
{
  int lineW = std::min(destStartX + width, dest.width) - destStartX;
  int linesCout = std::min(destStartY + height, dest.height) - destStartY;

  int copied = 0;

  for(int dstY = destStartY; dstY < destStartY + linesCout; ++dstY) {
    memcpy(&dest.image[(destStartX + dstY * dest.width) * 4 ], &image[copied], lineW * 4);
    copied += lineW * 4;
  }/*for (y)*/
}

void Texture::addBorder()
{
  std::vector<uint8_t> borderImage;
  int32_t bW = width + 2;
  int32_t bH = height + 2;
  borderImage.resize(bW * bH * 4);

  //upper left corner
  borderImage[0] = image[0];
  borderImage[1] = image[1];
  borderImage[2] = image[2];
  borderImage[3] = image[3];

  //copy of first line
  memcpy(&borderImage[4], &image[0], width * 4);

  //upper right corner
  borderImage[(bW - 1) * 4 + 0] = image[(width - 1) * 4 + 0];
  borderImage[(bW - 1) * 4 + 1] = image[(width - 1) * 4 + 1];
  borderImage[(bW - 1) * 4 + 2] = image[(width - 1) * 4 + 2];
  borderImage[(bW - 1) * 4 + 3] = image[(width - 1) * 4 + 3];

  //for each height in image
  for(int y = 0; y < height; ++y)
  {
    int bY = y + 1;
    //left frame pixel
    borderImage[bW * bY * 4 + 0] = image[width * y * 4 + 0];
    borderImage[bW * bY * 4 + 1] = image[width * y * 4 + 1];
    borderImage[bW * bY * 4 + 2] = image[width * y * 4 + 2];
    borderImage[bW * bY * 4 + 3] = image[width * y * 4 + 3];

    //image itself
    memcpy(&borderImage[(bW * bY + 1) * 4], &image[width * y * 4], width * 4);

    //right frame pixel
    borderImage[(bW * bY + 1 + width) * 4 + 0] = image[width * bY * 4 - 4];
    borderImage[(bW * bY + 1 + width) * 4 + 1] = image[width * bY * 4 - 3];
    borderImage[(bW * bY + 1 + width) * 4 + 2] = image[width * bY * 4 - 2];
    borderImage[(bW * bY + 1 + width) * 4 + 3] = image[width * bY * 4 - 1];

  }/*for (y)*/

  //bottom right corner
  borderImage[ bW * (bH - 1) * 4 + 0] = image[width * (height - 1) * 4 + 0];
  borderImage[ bW * (bH - 1) * 4 + 1] = image[width * (height - 1) * 4 + 1];
  borderImage[ bW * (bH - 1) * 4 + 2] = image[width * (height - 1) * 4 + 2];
  borderImage[ bW * (bH - 1) * 4 + 3] = image[width * (height - 1) * 4 + 3];

  //copy of last line
  memcpy(&borderImage[(bW * (bH - 1) + 1) * 4], &image[ width * (height - 1) * 4], width * 4);

  //left botton corner
  borderImage[ bW * bH * 4 - 4] = image[width * height * 4 - 4];
  borderImage[ bW * bH * 4 - 3] = image[width * height * 4 - 3];
  borderImage[ bW * bH * 4 - 2] = image[width * height * 4 - 2];
  borderImage[ bW * bH * 4 - 1] = image[width * height * 4 - 1];


  image.swap(borderImage);
  width = bW;
  height = bH;

  borderWidth += 1;
}

void Texture::makeShortFilename()
{
  size_t found = filename.find_last_of("\\/");
  if(found != std::string::npos && found < filename.size() - 1) {
    filename = filename.substr(found + 1);
  }
}

bool isPow2(uint32_t v)
{
  return (v & (v - 1)) == 0;
}

bool Texture::generateMipmap(Texture& outTex) const
{
  outTex.image.clear();
  int newWidth = nearestPow2(width) / 2;
  int newHeight = nearestPow2(height) / 2;
  assert(newWidth > 0 && newHeight > 0);
  if(newWidth < 1 || newHeight < 1)
    return false;

  if(!isPow2(width) || !isPow2(height))
    return generateMipmapEx(outTex);

  std::vector<uint8_t>& dest = outTex.image;
  outTex.allocate(newWidth, newHeight);
  outTex.filename = this->filename;

  for(int32_t iy = 0; iy < outTex.height; ++iy) 
  {
    for(int32_t ix = 0; ix < outTex.width; ++ix) 
    {
      int iy2 = 2 * iy;
      int ix2 = 2 * ix;

      const Pixel* a = pixelAt(ix2, iy2);
      const Pixel* b = pixelAt(ix2 + 2, iy2);
      const Pixel* c = pixelAt(ix2, iy2 + 2);
      const Pixel* d = pixelAt(ix2 + 2, iy2 + 2);

      for(int i = 0; i < 4; ++i)
      {
        dest.push_back(a->t[i] + b->t[i] + c->t[i] + d->t[i] / 4);
      }/*for (i)*/

      //destImage.image[iy*destImage.width + ix] = ul;
    }/*for (ix)*/

  }/*for (iy)*/

  return true;
}

bool Texture::generateMipmapEx(Texture& outTex, uint32_t newWidth, uint32_t newHeight) const
{
	//assert(newWidth > 0 && newHeight > 0);
	if(newWidth == 0 || newHeight == 0)
		return false;

	std::vector<uint8_t>& dest = outTex.image;
	dest.clear();

  outTex.filename = this->filename;
  outTex.borderWidth = 0;  
  outTex.allocate(newWidth, newHeight);

  float dTx = 1.f / (float)outTex.width;
  float dTy = 1.f / (float)outTex.height;

  float txY = 0.f;
  for(int iy = 0; iy < outTex.height; txY += dTy, ++iy) {
    float txX = 0.f;
    for(int ix = 0; ix < outTex.width; txX += dTx, ++ix)
    {
      const Pixel& px = calcPixel(txX, txY, txX + dTx, txY + dTy);
      dest.push_back(px.r);
      dest.push_back(px.g);
      dest.push_back(px.b);
      dest.push_back(px.a);
    }/*for (txX)*/
  }/*for (txY)*/

	return true;
}

bool Texture::generateMipmapEx(Texture& outTex, uint32_t level) const
{
	return generateMipmapEx(outTex, nearestPow2(width)/(1<<level), nearestPow2(height)/(1<<level) );
}

Pixel* Texture::pixelAt(uint32_t x, uint32_t y)
{
  return (Pixel*)&image[(y * width + x) * 4];
}

const Pixel* Texture::pixelAt(uint32_t x, uint32_t y) const
{
  return (Pixel*)&image[(y * width + x) * 4];
}

Pixel Texture::calcPixel(float texelX, float texelY) const
{
  return calcPixel(texelX, texelY, texelX + 1.f / (float)width, texelY + 1.f / (float)height);
}

static void getPixelColors(const Pixel& pixel,
                           uint32_t& r, uint32_t& g, uint32_t& b, uint32_t& a)
{
  r += pixel.r;
  g += pixel.g;
  b += pixel.b;
  a += pixel.a;
}

Pixel Texture::calcPixel(float texelBegX, float texelBegY, float texelEndX, float texelEndY) const
{
  Float2 begX(texelBegX * width);
  Float2 begY(texelBegY * height);

  Float2 endX(texelEndX * width);
  Float2 endY(texelEndY * height);

  float sumWeights = 0.f;
  float weight;
  Pixel tmp;
  uint32_t r = 0, g = 0, b = 0, a = 0;

  tmp = calcPixelLeftUp(begX, begY, weight);
  sumWeights += weight;
  getPixelColors(tmp, r, g, b, a);

  tmp = calcPixelRightUp(endX, begY, weight);
  sumWeights += weight;
  getPixelColors(tmp, r, g, b, a);

  tmp = calcPixelLeftDown(begX, endY, weight);
  sumWeights += weight;
  getPixelColors(tmp, r, g, b, a);

  tmp = calcPixelRightDown(endX, endY, weight);
  sumWeights += weight;
  getPixelColors(tmp, r, g, b, a);

  if(endX.i - begX.i > 1.f)
  {
    for(float ix = begX.i + 1.f; ix < endX.i; ++ix)
    {
      tmp = calcPixelUp(Float2(ix, 0.f), begY, weight);
      sumWeights += weight;
      getPixelColors(tmp, r, g, b, a);

      tmp = calcPixelDown(Float2(ix, 0.f), endY, weight);
      sumWeights += weight;
      getPixelColors(tmp, r, g, b, a);
    }/*for (ix)*/
  }

  if(endY.i - begY.i > 1.f)
  {
    for(float iy = begY.i + 1.f; iy < endY.i; ++iy)
    {
      tmp = calcPixelLeft(begX, Float2(iy, 0.f), weight);
      sumWeights += weight;
      getPixelColors(tmp, r, g, b, a);

      tmp = calcPixelRight(endX, Float2(iy, 0.f), weight);
      sumWeights += weight;
      getPixelColors(tmp, r, g, b, a);
    }/*for (iy)*/
  }

  if(endX.i - begX.i > 1.f && endY.i - begY.i > 1.f)
  {
    for(float iy = begY.i + 1.f; iy < endY.i; ++iy)
    {
      for(float ix = begX.i + 1.f; ix < endX.i; ++ix)
      {
        tmp = *pixelAt(ix, iy);
        sumWeights += 1.f;
        getPixelColors(tmp, r, g, b, a);
      }
    }
  }

  const float invWeights = 1.f / sumWeights;
  tmp.r = r * invWeights;
  tmp.g = g * invWeights;
  tmp.b = b * invWeights;
  tmp.a = a * invWeights;

  return tmp;
}

Pixel Texture::calcPixelLeftUp(Float2 texelX, Float2 texelY, float& weight) const
{
  weight = (1.f - texelX.f) * (1.f - texelY.f);
  return calcPixelPart(texelX.i, texelY.i, weight);
}

Pixel Texture::calcPixelRightUp(Float2 texelX, Float2 texelY, float& weight) const
{
  weight = texelX.f * (1.f - texelY.f);
  return calcPixelPart(texelX.i, texelY.i, weight);
}

Pixel Texture::calcPixelLeftDown(Float2 texelX, Float2 texelY, float& weight) const
{
  weight = (1.f - texelX.f) * texelY.f;
  return calcPixelPart(texelX.i, texelY.i, weight);
}

Pixel Texture::calcPixelRightDown(Float2 texelX, Float2 texelY, float& weight) const
{
  weight = texelX.f * texelY.f;
  return calcPixelPart(texelX.i, texelY.i, weight);
}


Pixel Texture::calcPixelUp(Float2 texelX, Float2 texelY, float& weight) const
{
  weight = 1.f - texelY.f;
  return calcPixelPart(texelX.i, texelY.i, weight);
}

Pixel Texture::calcPixelRight(Float2 texelX, Float2 texelY, float& weight) const
{
  weight = texelX.f;
  return calcPixelPart(texelX.i, texelY.i, weight);
}

Pixel Texture::calcPixelDown(Float2 texelX, Float2 texelY, float& weight) const
{
  weight = texelY.f;
  return calcPixelPart(texelX.i, texelY.i, weight);
}

Pixel Texture::calcPixelLeft(Float2 texelX, Float2 texelY, float& weight) const
{
  weight = 1.f - texelX.f;
  return calcPixelPart(texelX.i, texelY.i, weight);
}


Pixel Texture::calcPixelPart(uint32_t pixelX, uint32_t pixelY, float weight) const
{
  if(weight == 0.f)
  {
    // Here pixelX or pixelY may be out of image range.
    return Pixel(0);
  }
  if(pixelX >= width)
  {
    pixelX = width - 1;
  }
  if(pixelY >= height)
  {
    pixelY = height - 1;
  }
  
  Pixel px = *pixelAt(pixelX, pixelY);
  for(int i = 0; i < 4; ++i)
  {
    px.t[i] *= weight;
  }/*for (i)*/
	
  return px;
}

uint32_t Texture::to1d(uint32_t x, uint32_t y)
{
  return (y * width + x) * 4;
}


enum 
{
  LEAF,
  NODE,
};

Node::Node()
{
  child[0] = child[1] = 0;
  state = LEAF;
  image = 0;
}

Node::~Node()
{
  delete child[0];
  delete child[1];
}

//http://www.blackpawn.com/texts/lightmaps/default.html
//osom :P
Node* Node::insert(Texture* img)
{
  if(state != LEAF)
  {
    Node* newNode = child[0]->insert(img);
    if(newNode != 0)
      return newNode;
    //no room, insert into second
    return child[1]->insert(img);
  }
  else
  {
    //there's alrady texture in this node
    if(image != 0)
      return 0;

    //node too small for texture
    if((img->width > rect.w) || (img->height > rect.h))
      return 0;

    //texture fits to node
    if((img->width == rect.w) && (img->height == rect.h))
    {
      image = img;
      state = LEAF;
      return this;
    }

    //node too big for texture,
    //continue splitting...

    state = NODE;

    child[0] = new Node;
    child[1] = new Node;

    int dw = rect.w - img->width;
    int dh = rect.h - img->height;

    if(dw > dh)
    { //wider than higher
      //horizontal split

      //left child
      child[0]->rect = RectI { rect.x, rect.y, img->width, rect.h };
      //right child
      child[1]->rect = RectI { rect.x + img->width, rect.y, rect.w - img->width, rect.h};
    }
    else
    {
      //vertical split
      //top child
      child[0]->rect = RectI { rect.x, rect.y, rect.w, img->height };
      //bottom child
      child[1]->rect = RectI { rect.x, rect.y + img->height, rect.w, rect.h - img->height };
    }

    //and insert into splitted child
    return child[0]->insert(img);
  }
}


//=== Atlas

Atlas::Atlas() : 
  tiles(),
	sumHeights(0),
	maxWidth(0),
	flags(0),
  imgOk(false),
  treeOk(false)
{

}

void Atlas::setFlags(uint32_t a_flags, bool state)
{
  treeOk = false;
  imgOk = false;

  if(state)
  {
    flags |= a_flags;
  }
  else
  {
    flags &= ~a_flags;
  }

}

void Atlas::insertTexture(const std::string& filename)
{
  tiles.push_back(Tile());
  Tile& tmp = tiles.back();
  if(!tmp.loadPng(filename.c_str())) 
	{
    tiles.pop_back();
    return;
  }

  if(flags & ADD_BORDER)
    tmp.addBorder();

  sumHeights += tmp.height;
  maxWidth = std::max(tmp.width, maxWidth);

  imgOk = false;
  treeOk = false;
}

void Atlas::getBinaryTexcoords(std::vector<uint8_t>& binInfo)
{
  createTree();

  binInfo.clear();

  float texelW = 1.f / float(width);
  float texelH = 1.f / float(height);
  Header h = getHeader(texelW, texelH);

  writeValue(binInfo, (uint32_t)tiles.size());//tiles count
  writeValue(binInfo, h.imgWidth);
  writeValue(binInfo, h.imgHeight);
  writeValue(binInfo, h.txOffsetX);
  writeValue(binInfo, h.txOffsetY);
  writeValue(binInfo, h.flags);
  writeValue(binInfo, '\n');

  for(uint32_t i = 0; i < tiles.size(); ++i)
  {
    binInfo.push_back('\"');
    for(uint32_t c = 0; c < tiles[i].filename.size(); ++c) {
      binInfo.push_back(tiles[i].filename[c]);
    }/*for (c)*/

    binInfo.push_back('\"');
    binInfo.push_back(0);    //null termination

    float texels[4];
    getTexels(tiles[i], texels, texelW, texelH, h.txOffsetX, h.txOffsetY);

    writeValue(binInfo, texels[0]);
    writeValue(binInfo, texels[1]);
    writeValue(binInfo, texels[2]);
    writeValue(binInfo, texels[3]);

    writeValue(binInfo, '\n');
  }/*for (i)*/

}

void Atlas::getTextTexcoords(std::string& textInfo)
{
  createTree();

  textInfo = "#header:\n";
  textInfo += "# tiles count, image width, image height, x texel offset, y texel offset, flags\n";

  float texelW = 1.f / float(width);
  float texelH = 1.f / float(height);
  Header h = getHeader(texelW, texelH);

  textInfo += format("%u %u %u %f %f %u\n", h.tilesCount, h.imgWidth, h.imgHeight,
                     h.txOffsetX, h.txOffsetY, h.flags);

  textInfo += "# <filename>\t\t<x offset> <y offset> <width> <height> \n";

  for(uint32_t i = 0; i < tiles.size(); ++i) {
    textInfo += '\"';
    textInfo += tiles[i].filename;
    textInfo += "\"\t";

    float texels[4];
    getTexels(tiles[i], texels, texelW, texelH, h.txOffsetX, h.txOffsetY);

    textInfo += format("%.10f %.10f %.10f %.10f\n",
                       texels[0], texels[1], texels[2], texels[3]);
  }/*for (i)*/
}

void Atlas::saveBinaryTexcoords(const char* filename)
{
  std::vector<uint8_t> binInfo;
  getBinaryTexcoords(binInfo);

  printf("saving texcoord information to binary file \"%s\"\n", filename);
  FILE* file = fopen(filename, "wb");
  if(!file)
    critError("failed to create binary file");
  fwrite(&binInfo[0], 1, binInfo.size(), file);
  fclose(file);
}

void Atlas::saveTextTexcoords(const char* filename)
{
  std::string textInfo;
  getTextTexcoords(textInfo);

  printf("saving texcoord information to text file \"%s\"\n", filename);
  FILE* file = fopen(filename, "w");
  if(!file)
    critError("failed to create text file");
  fputs(textInfo.c_str() , file);
  fclose(file);

}

const std::vector<uint8_t>& Atlas::getRGBA()
{
  createImg();
  return this->image;
}

void Atlas::saveImage(const char* filename)
{
  this->filename = filename;
  createImg();

  savePng();
}

bool Atlas::generateMipmap(uint32_t level, Texture& outTex)
{
	createTree();

	int div = (1<<(level+1));
	int newWidth = nearestPow2(width)/div;
	int newHeight = nearestPow2(height)/div;
	
	if( newWidth <= 0 || newHeight <= 0)
		return false;
	
  outTex.image.clear();
  
  outTex.allocate(newWidth, newHeight);
  outTex.image.resize(newWidth*newHeight*4);

  for(uint32_t i = 0; i < tiles.size(); ++i)
  {
    assert(tiles[i].rect.x + tiles[i].rect.w <= width);
    assert(tiles[i].rect.y + tiles[i].rect.h <= height);
    
    Texture tmpTex;
    if( tiles[i].generateMipmapEx(tmpTex, tiles[i].width/div, tiles[i].height/div) )
		{
			tmpTex.blitTo(outTex, tiles[i].rect.x/div, tiles[i].rect.y/div);
		}
  }
  return true;
}

static
bool byWidth(const Atlas::Tile& l, const Atlas::Tile& r)
{
  return l.width > r.width;
}

void Atlas::createTree()
{
  if(treeOk)
    return;

  std::sort(tiles.begin(), tiles.end(), byWidth);

  adjustDimension();

  root.~Node();
  new(&root) Node();

  root.rect = RectI {0, 0, maxWidth, sumHeights};
  //width = maxWidth;
  //height = sumHeights;

  for(uint32_t i = 0; i < tiles.size(); ++i) 
	{
    Node* node = root.insert(&tiles[i]);
    if(node)
    {
      RectI& rect = node->rect;

      width = std::max(width, rect.x + rect.w);
      height = std::max(height, rect.y + rect.h);
      tiles[i].rect = rect;
    }
  }/*for (i)*/

  width = nearestPow2(width);
  height = nearestPow2(height);

  treeOk = true;
}

void Atlas::createImg()
{
  if(imgOk)
    return;

  createTree();

  image.clear();
  allocate(width, height);
  image.resize(width*height*4);

  for(uint32_t i = 0; i < tiles.size(); ++i)
  {
    assert(tiles[i].rect.x + tiles[i].rect.w <= width);
    assert(tiles[i].rect.y + tiles[i].rect.h <= height);
    tiles[i].blitTo(*this, tiles[i].rect.x, tiles[i].rect.y);
  }

  imgOk = true;
}

Header Atlas::getHeader(float texelW, float texelH) const
{

  if(flags & REDUCE_BY_HALF_TEXEL)
  {
    texelW *= 0.5f;
    texelH *= 0.5f;
  }
  else
  {
    texelW = 0.f;
    texelH = 0.f;
  }

  Header h { (uint32_t)tiles.size(), (uint32_t)width, (uint32_t)height, texelW, texelH , flags, '\n' };
  return h;
}

float* Atlas::getTexels(const Tile& tile, float txCoord[4], float texelWidth, float texelHeight,
                        float txOffsetX, float txOffsetY) const
{
  //texelWidth = 1.f / float(width);
  //texelHeight = 1.f / float(height);

  txCoord[0] = (tile.rect.x + borderWidth) * texelWidth;
  txCoord[1] = (tile.rect.y + borderWidth) * texelHeight;
  txCoord[2] = (tile.rect.w - 2 * borderWidth) * texelWidth;
  txCoord[3] = (tile.rect.h - 2 * borderWidth) * texelHeight;

  txCoord[0] += txOffsetX;
  txCoord[1] += txOffsetY;
  txCoord[2] -= 2.f * txOffsetX;
  txCoord[3] -= 2.f * txOffsetY;

  return txCoord;
}

void Atlas::adjustDimension()
{
  //make atlas more 'squared':
  maxWidth = nearestPow2(maxWidth);
  sumHeights = nearestPow2(sumHeights);
  while(sumHeights > maxWidth) {
    sumHeights = sumHeights >> 1; // /2
    maxWidth = maxWidth << 1; // *2
  }
}
