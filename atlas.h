/*
Copyright (c) 2013 Paweł Turkowski

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
*/

#pragma once
#include "precompiled.h"

#include <vector>
#include <cstdint>

#include "common.h"

namespace atlas
{
  struct Pixel
  {
    Pixel() = default;
    Pixel(uint32_t rgba)
    {
      v = rgba;
    }
    
    union
    {
      struct {
        uint8_t r;
        uint8_t g;
        uint8_t b;
        uint8_t a;
      };
      uint32_t v;
      uint8_t t[4];
    };
  };

  struct Float2
  {
    Float2();
    Float2(float a);
    Float2(float integral, float factorial);

    float i; //integral
    float f; //fractional
  };

  struct Texture
  {
    Texture();

    bool loadPng(const char* filename);
    void savePng(const char* filename = 0);

    void allocate(int width, int height);

    void blitTo(Texture& dest, int32_t destStartX, int32_t destStartY);

    void addBorder();

    void makeShortFilename();

    bool generateMipmap(Texture& outTex) const;
    bool generateMipmapEx(Texture& outTex, uint32_t width, uint32_t height) const;
    bool generateMipmapEx(Texture& outTex, uint32_t level = 1) const;

    Pixel* pixelAt(uint32_t x, uint32_t y);
    const Pixel* pixelAt(uint32_t x, uint32_t y) const;

    Pixel calcPixel(float texelX, float texelY) const;
    Pixel calcPixel(float texelBegX, float texelBegY, float texelEndX, float texelEndY) const;

    Pixel calcPixelLeftUp(Float2 texelX, Float2 texelY, float& weight) const;
    Pixel calcPixelRightUp(Float2 texelX, Float2 texelY, float& weight) const;
    Pixel calcPixelLeftDown(Float2 texelX, Float2 texelY, float& weight) const;
    Pixel calcPixelRightDown(Float2 texelX, Float2 texelY, float& weight) const;

    Pixel calcPixelUp(Float2 texelX, Float2 texelY, float& weight) const;
    Pixel calcPixelRight(Float2 texelX, Float2 texelY, float& weight) const;
    Pixel calcPixelDown(Float2 texelX, Float2 texelY, float& weight) const;
    Pixel calcPixelLeft(Float2 texelX, Float2 texelY, float& weight) const;

    Pixel calcPixelPart(uint32_t pixelX, uint32_t pixelY, float weight) const;

    uint32_t to1d(uint32_t x, uint32_t y);

    std::vector<uint8_t> image;
    int32_t width, height;
    std::string filename;
    int borderWidth;
  };

  struct Node
  {
    Node();
    ~Node();

    Node* insert(Texture* img);

    Node* child[2];
    RectI rect;
    Texture* image;
    int state;
  };

  struct Header
  {
    uint32_t tilesCount;
    uint32_t imgWidth, imgHeight;
    float txOffsetX, txOffsetY;
    uint32_t flags;
    char newLine;
  };

  struct Atlas : protected Texture
  {

    struct Tile : Texture
    {
      RectI rect;
    };
    enum
    {
      REDUCE_BY_HALF_TEXEL = 1 << 1,
      ADD_BORDER = 1 << 2,
    };

    Atlas();

    void setFlags(uint32_t a_flags, bool state);

    void insertTexture(const std::string& filename);

		//with header
    void getBinaryTexcoords(std::vector<uint8_t>& binInfo);
    //with header
    void getTextTexcoords(std::string& textInfo);

    void saveBinaryTexcoords(const char* filename);
    void saveTextTexcoords(const char* filename);

    const std::vector<uint8_t>& getRGBA();

    void saveImage(const char* filename);

    bool generateMipmap(uint32_t level, Texture& outTex);

  protected:
    void createTree();
    void createImg();
    void adjustDimension();
    Header getHeader(float texelW, float texelH) const;
    float* getTexels(const Tile& tile, float txCoord[4], float texelWidth, float texelHeight,
                     float txOffsetX, float txOffsetY) const;

  public:
    std::vector<Tile> tiles;
    int32_t sumHeights, maxWidth;

  private:
    uint32_t flags;
    bool imgOk, treeOk;

    Node root;
  };

}//namespace atlas
