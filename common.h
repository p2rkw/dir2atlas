/*
Copyright (c) 2013 Paweł Turkowski

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
*/

#pragma once

#include <vector>

template <class T>
inline
int loadFile(const char* name, std::vector<T>& buffer)
{
  FILE* pFile = fopen(name, "rb");
  if(!pFile)
    return -1;
  fseek(pFile, 0, SEEK_END);
  const int32_t bFileSize = (ftell(pFile) / sizeof(T));
  fseek(pFile, 0, SEEK_SET);

  //even if buffer will be null terminated outside this function
  //memory will not be reallocated.
  buffer.reserve(bFileSize + sizeof(T));
  buffer.resize(bFileSize);

  size_t result = fread(&buffer[0], sizeof(T), bFileSize, pFile);

  fclose(pFile);

  return result / sizeof(T);
}

inline
void critError(const char* msg)
{
  puts(msg);
  getchar();
  //size_t fread ( void * ptr, size_t size, size_t count, FILE * stream );
  //setvbuf ( stdin, NULL , _IONBF , 1 );
  //char bfr[2];
  //fread( bfr, 1,1, stdin );
  exit(EXIT_FAILURE);
}

struct PointI
{
  PointI() : x(0), y(0) {}
  PointI(int ix, int iy) : x(ix), y(iy) {}
  PointI(const PointI& p) : x(p.x), y(p.y) {}

  const PointI& operator= (const PointI& p) {
    this->x = p.x;
    this->y = p.y;
    return *this;
  }

  const PointI& operator+ (const PointI& p) {
    this->x += p.x;
    this->y += p.y;
    return *this;
  }
  bool operator<= (const PointI& p) const {
    return this->x <= p.x && this->y <= p.y;
  }
  bool operator< (const PointI& p) const {
    return this->x < p.x && this->y < p.y;
  }

  int x, y;
};

struct RectI
{
  static const RectI null;

  RectI() :
    x(0),
    y(0),
    w(0),
    h(0)
  {}
  
  //RectI( const PointI& p ) : x( p.x ), y( p.y ), w( 0 ), h( 0 ) {}
  RectI(int ix, int iy, int iw = 0, int ih = 0) :
    x(ix),
    y(iy),
    w(iw),
    h(ih)
  {}

  RectI(const RectI& r) :
    x(r.x),
    y(r.y),
    w(r.w),
    h(r.h)
  {}

  const RectI& operator= (const RectI& r)
  {
    this->x = r.x;
    this->y = r.y;
    this->w = r.w;
    this->h = r.h;
    return *this;
  }

  PointI upLeft() const
  {
    return PointI(x, y);
  }

  PointI downRight() const
  {
    return PointI(x + w, y + h);
  }

  int x, y;
  int w, h;
};



inline
char* format(const char* text, ...) {
  static char arr[BUFSIZ];
  if(text) {
    memset(arr, 0, BUFSIZ);
    va_list args;
    va_start(args, text);
    vsprintf(arr, text, args);
    va_end(args);
  }
  return arr;
}


template <class T>
inline
uint32_t writeValue(std::vector<uint8_t>& v, T t)
{
  uint8_t* tmp = (uint8_t*)(&t);
  v.insert(v.end(), tmp, tmp + sizeof(T));
  return sizeof(T);
}

template<class T>
inline
uint32_t readValue(T* dest, const char* v)
{
  *dest = *(T*)(v);
  return sizeof(T);
}


inline uint32_t nearestPow2(uint32_t x)
{
  --x;
  x |= x >> 1;
  x |= x >> 2;
  x |= x >> 4;
  x |= x >> 8;
  x |= x >> 16;
  return ++x;
}
